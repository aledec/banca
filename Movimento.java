import java.util.Date;

public class Movimento {
    private Date dataRichiesta;
    private Date dataValuta;
    private String causale;
    private String tipologia;
    private double importo;

    public Movimento(Date dataRichiesta, Date dataValuta, String causale, String tipologia, double importo) {
        this.dataRichiesta = dataRichiesta;
        this.dataValuta = dataValuta;
        this.causale = causale;
        this.tipologia = tipologia;
        this.importo = importo;
    }

    public Date getDataRichiesta() {
        return dataRichiesta;
    }

    public Date getDataValuta() {
        return dataValuta;
    }

    public String getCausale() {
        return causale;
    }

    public String getTipologia() {
        return tipologia;
    }

    public double getImporto() {
        return importo;
    }

    public void setImporto(double importo) {
        this.importo = importo;
    }
}