import java.util.ArrayList;
import java.util.Date;

public class ContoCorrente {
    private double saldo;
    private boolean contoBloccato;
    private boolean fidoAttivo; 
    private ArrayList<Movimento> movimenti;
    private Persona intestatario;

    public ContoCorrente(double saldoIniziale, Persona intestatario) {
        this.saldo = saldoIniziale;
        this.contoBloccato = false;
        this.fidoAttivo = true; 
        this.movimenti = new ArrayList<>();
        this.intestatario = intestatario;
    }

    public double getSaldo() {
        return saldo;
    }

    public boolean isContoBloccato() {
        return contoBloccato;
    }

    public void bloccaConto() {
        this.contoBloccato = true;
        System.out.println("Il conto è stato bloccato su richiesta dell'Autorità giudiziaria.");
    }

    public void attivaFido() {
        fidoAttivo = true;
        System.out.println("Fido attivato. Puoi prelevare anche con saldo negativo.");
    }

    public void disattivaFido() {
        fidoAttivo = false;
        System.out.println("Fido disattivato.");
    }

    public void prelievo(double importo, String causale) {
        if (!contoBloccato && (fidoAttivo || importo <= saldo) && importo > 0) {
            saldo -= importo;
            Date dataRichiesta = new Date();
            Date dataValuta = new Date();
            Movimento movimento = new Movimento(dataRichiesta, dataValuta, causale, "Prelievo", importo);
            movimenti.add(movimento);
            System.out.println("Prelievo avvenuto con successo. Saldo rimanente: " + saldo);
        } else if (contoBloccato) {
            System.out.println("Il conto è bloccato. Impossibile effettuare prelievi.");
        } else {
            System.out.println("Importo non valido o saldo insufficiente.");
        }
    }

    public void deposito(double importo, String causale) {
        if (!contoBloccato && importo > 0) {
            saldo += importo;
            Date dataRichiesta = new Date();
            Date dataValuta = new Date();
            Movimento movimento = new Movimento(dataRichiesta, dataValuta, causale, "Deposito", importo);
            movimenti.add(movimento);
            System.out.println("Deposito avvenuto con successo. Nuovo saldo: " + saldo);
        } else if (contoBloccato) {
            System.out.println("Il conto è bloccato. Impossibile effettuare depositi.");
        } else {
            System.out.println("Importo non valido per il deposito.");
        }
    }

    public void stampaMovimenti() {
        System.out.println("===== MOVIMENTI =====");
        int numeroMovimento = 1;
        for (Movimento movimento : movimenti) {
            System.out.println("---- Movimento " + numeroMovimento + " ----");
            System.out.println("Tipologia: " + movimento.getTipologia());
            System.out.println("Importo: " + movimento.getImporto());
            System.out.println("Data richiesta: " + movimento.getDataRichiesta());
            System.out.println("Data valuta: " + movimento.getDataValuta());
            System.out.println("Causale: " + movimento.getCausale());
            numeroMovimento++;
        }
    }
}